$( document ).ready( function() {
    $( "#filterButton" ).click( function() {
        BeginFilter();
    } );
    
    $( document ).keypress( function( ev ) {
        if ( ev.keyCode == 13 ) {
            BeginFilter();
        }
    } );
    
    function BeginFilter() {
        $( "#filterButton" ).html( "Searching..." );
        $( "#filterButton" ).removeClass( "btn-success" );
        $( "#filterButton" ).addClass( "btn-warning" );
        
        setTimeout( function() {
            Filter();
        }, 1000 );
    }
    
    function EndFilter() {
        $( "#filterButton" ).html( "Filter" );
        $( "#filterButton" ).addClass( "btn-success" );
        $( "#filterButton" ).removeClass( "btn-warning" );
    }
    
    function Filter() {
    }
    
    function Sanitize( str )
    {
        str = str.toLowerCase();
        str = str.replace( "á", "a" );
        str = str.replace( "é", "e" );
        str = str.replace( "í", "i" );
        str = str.replace( "ó", "o" );
        str = str.replace( "ú", "u" );
        return str;
    }

    function Hide( element )
    {
        $( element ).css( "display", "none" );
    }

    function Show( element )
    {
        $( element ).css( "display", "table-row" );
    }
    

    function ShowAll()
    {
        var classification = $('#find-what').find(":selected").text();
        if ( classification == "all classifications" ) { classification = ""; }
        
        $.each( $( ".dictionary-entry" ), function( key, element ) {
            var entryClass          = Sanitize( $( element ).attr( "classification" ) );
            if ( classification == "" || StringContains( entryClass, classification )  )
            {
                Show( element );
            }
            else
            {
                Hide( element );
            }
        } );

        EndFilter();
    }

    function StringContains( fullString, subString )
    {
        return ( fullString.indexOf( subString ) >= 0 );
    }
    
    
    function Filter()
    {
        var searchTerm = Sanitize( $( "#searchTerm" ).val() );
        
        if ( searchTerm == "" )
        {
            ShowAll();
            return;
        }
        
        var checkWhat = "all";
        if      ( $( "#search-laadan" ).prop( "checked" ) == true )         { checkWhat = "laadan"; }
        else if ( $( "#search-english" ).prop( "checked" ) == true )        { checkWhat = "english"; }
        else if ( $( "#search-espanol" ).prop( "checked" ) == true )        { checkWhat = "espanol"; }

        var classification = $('#find-what').find(":selected").text();
        if ( classification == "all classifications" ) { classification = ""; }

        var itemCount = 0;
        var shown = 0;
        var hidden = 0;
        
        $.each( $( ".dictionary-entry" ), function( key, element ) {
            var entryLaadan         = Sanitize( $( element ).attr( "laadan" ) );
            var entryEnglish        = Sanitize( $( element ).attr( "english" ) );
            var entryEspanol        = Sanitize( $( element ).attr( "espanol" ) );
            var entryClass          = Sanitize( $( element ).attr( "classification" ) );
            var entryDesc           = Sanitize( $( element ).attr( "description" ) );
            
            var languageText = "";
            if ( checkWhat == "all" ) {
                languageText = entryLaadan + " " + entryEnglish + " " + entryEspanol + " " + entryDesc;
            }
            else if ( checkWhat == "laadan" ) {
                languateText = entryLaadan;
            }
            else if ( checkWhat == "english" ) {
                languateText = entryEnglish;
            }
            else if ( checkWhat == "espanol" ) {
                languateText = entryEspanol;
            }
            
            itemCount++;

            if ( checkWhat == "all" )
            {
                if (
                    ( StringContains( entryLaadan, searchTerm ) ||
                    StringContains( entryEnglish, searchTerm ) ||
                    StringContains( entryEspanol, searchTerm ) ||
                    StringContains( entryDesc, searchTerm ) 
                    ) &&
                    ( classification == "" || StringContains( entryClass, classification ) )
                    ) {
                    Show( element );
                    shown++;
                }
                else
                {
                    Hide( element );
                    hidden++;
                }
            }
            else if ( checkWhat == "laadan" )
            {
                if (
                    StringContains( entryLaadan, searchTerm ) &&
                    ( classification == "" || StringContains( entryClass, classification ) )
                    ) {
                    Show( element );
                    shown++;
                }
                else {
                    Hide( element );
                    hidden++;
                }
            }
            else if ( checkWhat == "english" )
            {
                if ( 
                    StringContains( entryEnglish, searchTerm ) &&
                    ( classification == "" || StringContains( entryClass, classification ) )
                    ) { 
                    Show( element );
                    shown++;
                }
                else {
                    Hide( element );
                    hidden++;
                }
            }
            else if ( checkWhat == "espanol" )
            {
                if ( 
                    StringContains( entryEspanol, searchTerm ) &&
                    ( classification == "" || StringContains( entryClass, classification ) )
                    ) { 
                    Show( element );
                    shown++;
                }
                else {
                    Hide( element );
                    hidden++;
                }
            }
        } ); // each

        EndFilter();
    }
} );
