<?

class Dictionary
{
    private $laadanDictionary = array();
    public $log = array();
    
    function __construct() {
        $this->LoadDictionary();
        
        $changes = true;
        if ( isset( $_POST["dictionary-update"] ) ) {
            $this->UpdateDictionary( $_POST["item"] );
        }
        else if ( isset( $_POST["dictionary-add"] ) ) {
            $this->AddToDictionary( $_POST["item"] );
        }
        else if ( isset( $_POST["dictionary-delete"] ) ) {
            $this->RemoveFromDictionary( $_POST["item"] );
        }
        else { 
            $changes = false;
        }
        
        if ( $changes ) {
            // Reload latest
            $this->LoadDictionary();
        }
    }

    function Log( $data ) {
        array_push( $this->log, $data );
    }

    function LoadDictionary() {
        $this->Log( "LoadDictionary" );
        $path = "data-laadan-dictionaries/multilingual_laadan_dictionary-webedit.json";
        $jsonString = file_get_contents( $path );
        $this->laadanDictionary = json_decode( $jsonString, true );
    }
    
    function UpdateDictionary( $array ) {
        $this->Log( "UpdateDictionary" );
        // Update one entry
        foreach( $array as $laadan => $data ) {
            $this->Log( "Update entry for " . $laadan );
            $this->laadanDictionary[ $laadan ] = $data;
        }
        $this->SaveDictionary();
    }
    
    function AddToDictionary( $array ) {
        $this->Log( "AddToDictionary" );
        $newLaadan = $array["new"]["láadan"];
        $this->laadanDictionary[$newLaadan] = $array["new"];
        $this->SaveDictionary();
    }
    
    function RemoveFromDictionary( $array ) {
        $this->Log( "RemoveFromDictionary" );
        // Update one entry
        foreach( $array as $laadan => $data ) {
            $this->Log( "Delete entry for " . $laadan );
            unset( $this->laadanDictionary[ $laadan ] );
        }
        $this->SaveDictionary();
    }

    function SaveDictionary() {
        $this->Log( "SaveDictionary" );
        $path = "data-laadan-dictionaries/multilingual_laadan_dictionary-webedit.json";
        $jsonString = json_encode( $this->GetDictionary() );
        file_put_contents( $path, $jsonString );
    }

    function GetDictionary() {
        return $this->laadanDictionary;
    }
};

$dictionary = new Dictionary();

?>
