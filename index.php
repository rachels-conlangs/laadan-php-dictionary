<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> Láadan Dictionary </title>
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">

  <link rel="stylesheet" href="assets/style.css">
  <link rel="icon" type="image/png" href="assets/favicon.png">

  <script src="assets/jquery-3.5.0.min.js"></script>
  <script src="assets/script.js"></script>
</head>

<body>
    
<? include_once( "backend.php" ); ?>

<div class="dictionary-view">
    <h1>Láadan Dictionary</h1>
    <div class="more-links">
        <p>
            <a href="https://en.wikibooks.org/wiki/L%C3%A1adan">Láadan Wikibook</a> |
            <a href="http://tools.ayadanconlangs.com/laadan-accenter/">Letter Accenter</a> |
            <a href="http://tools.ayadanconlangs.com/sentence-builder/">Sentence Builder</a>
        </p>
    </div>
    <div class="combined-view cf">
    
        <div class="about-view">
            <div class="padding">
                <p>
                    This is a filterable Láadan dictionary that uses the official
                    Láadan dictionary from <a href="http://laadanlanguage.wordpress.com">laadanlanguage.wordpress.com</a>
                    as well as added words from the community.
                </p>
                <p>
                    The data files were parsed and the dictionary was built by Rachel Wil Sha Singh. rachel@moosader.com<br>
		    The Spanish language files were translated by Tea Duckie
                </p>
                <p>
                    Source code repositories: <br>
                    <a href="https://gitlab.com/aya-dan-conlangs/laadan-php-dictionary">laadan php dictionary</a> <br>
                    <a href="https://gitlab.com/aya-dan-conlangs/data-laadan-dictionaries">data-laadan-dictionaries</a>
                </p>
            </div>
        </div>
        
        <div class="search-area">
            <div class="search-classification cf">
                <div class="col search-term">
                    <sub>Search term(s)</sub><br>
                    <input type="text" id="searchTerm" placeholder="Search term(s)" class="search-terms">
                </div>
            
                <div class="col classification">
                    <sub>Classification</sub><br>
                    <select id="find-what" class="classification">
                        <option>all classifications</option>
                        <option>noun</option>
                        <option>core word</option>
                        <option>transitive verb</option>
                        <option>intransitive verb</option>
                        <option>speech act morpheme</option>
                        <option>evidence morpheme</option>
                        <option>affix</option>
                        <option>marker</option>
                        <option>first declension</option>
                        <option>second declension</option>
                    </select>
                </div>
            </div>
            
            <hr>
            
            <div class="radio-buttons cf">
                <div class="radio">
                    <input type="radio" name="search-type" id="search-all" value="all" checked="checked">
                    <label for="search-all">Search all</label>
                </div>
                
                <div class="radio">
                    <input type="radio" name="search-type" id="search-laadan" value="all">
                    <label for="search-laadan">Search only Láadan</label>
                </div>
                    
                <div class="radio">
                    <input type="radio" name="search-type" id="search-english" value="all">
                    <label for="search-english">Search only English</label>
                </div>
                    
                <div class="radio">
                    <input type="radio" name="search-type" id="search-espanol" value="all">
                    <label for="search-espanol">Search only Español</label>
                </div>
            </div>
            
            <hr>
            <input type="button" id="filterButton" value="Filter" class="filter-button btn-ready">
        </div>
        
    </div> <!-- Combined view -->
    
    <div class="results-view" id="results-view">
        <table>
            <thead>
                <tr>
                    <th class="laadan">Láadan</th>
                    <th class="language">English</th>
                    <th class="language">Español</th>
                    <th class="description">Description</th>
                    <th class="classification">Classification</th>
                </tr>
            </thead>
            
            <tbody>
            
            <? foreach( $dictionary->GetDictionary() as $key => $item ) { ?>
                <tr class="dictionary-entry" english="<?=$item["english"]["translation"]?>" laadan="<?=$item["láadan"]?>" espanol="<?=$item["español"]["translation"]?>" description="<?=$item["english"]["description"]?>" classification="<?=$item["english"]["classification"] ?>">
                    <td class="laadan">
                        <?=$item["láadan"]?>
                    </td>
                    
                    <td class="language english">
                        <?=$item["english"]["translation"]?>
                    </td>
                    
                    <td class="language espanol">
                        <?=$item["español"]["translation"]?>
                    </td>
                    
                    <td class="description">
                        <p class="description"><?=$item["english"]["description"]?></p>
                        <? if ( $item["english"]["breakdown"] != "" ) { ?>
                        <div class="breakdown">
                            <?=$item["english"]["breakdown"]?>
                        </div>
                        <? } ?>
                        
                        <? if ( $item["english"]["notes"] != "" ) { ?>
                        <div class="notes">
                            <?=$item["english"]["notes"]?>
                        </div>
                        <? } ?>
                    </td>
                    
                    <td class="classification">
                        <?=$item["english"]["classification"] ?>
                    </td>
                </tr>
            <? } ?>
            </tbody>
        </table>
        
    </div>
</div>

</body>
</html>
